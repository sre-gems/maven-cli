#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mavencli/version'

Gem::Specification.new do |s|
  s.name = 'maven-cli'
  s.version = MavenCLI::VERSION
  s.authors = ['Samuel Bernard', 'Richard Delaplace']
  s.email = 'dps.team@s4m.io'
  s.license = 'Apache-2.0'

  s.summary = 'Tool to interact with Maven repositories with CLI'
  s.description = 'Tool to interact with Maven repositories with CLI'
  s.homepage = 'https://gitlab.com/s4m-gems/maven-cli'

  s.files = `git ls-files`.lines.map(&:chomp)
  s.bindir = 'bin'
  s.executables = `git ls-files bin/*`.lines.map do |exe|
    File.basename(exe.chomp)
  end
  s.require_paths = ['lib']

  s.required_ruby_version = '>= 2.0'

  s.add_development_dependency 'bundler', '~> 1.12'
  s.add_development_dependency 'rspec', '~> 3.5'
  s.add_development_dependency 'rake', '~> 11.2'
  s.add_development_dependency 'rubocop', '~> 0.43'
  s.add_development_dependency 'webmock', '~> 2.1'
  s.add_development_dependency 'simplecov', '~> 0.12'

  s.add_dependency 'thor', '~> 0.19'
  s.add_dependency 'xml-simple', '~> 1.1'
  s.add_dependency 'ruby-progressbar', '~> 1.8'
  s.add_dependency 'devnull', '~> 0.1.2'
end
