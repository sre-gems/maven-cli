#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

nexus = 'https://nexus.mydomain.net/repository/maven-public'
redirect = 'https://toredirect.org/maven'

def dl_test(file)
  dir = File.dirname(__FILE__)
  File.join(dir, '..', '..', file)
end

describe MavenCLI::Downloader do # rubocop:disable Metrics/BlockLength
  context 'download com/mygroup test 1.0.0' do
    it 'downloads without option.' do
      file = 'test-1.0.0.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test 2.0.0 -s' do
    it 'should simulate a download.' do
      out = "test-2.0.0.jar has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context "download com/mygroup test 3.0.0 -r #{nexus}" do
    it 'downloads with option --repository.' do
      file = 'test-3.0.0.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test 4.0.0 -p tgz' do
    it 'downloads with option --packaging.' do
      file = 'test-4.0.0.tgz'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test 5.0.0 -o test-5.0.0_test.jar' do
    it 'downloads with option --output-file.' do
      file = 'test-5.0.0_test.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test 6.0.0 -q' do
    it 'downloads with option --quiet.' do
      file = 'test-6.0.0.jar'
      expect { start(self) }.to output('').to_stdout
      File.delete(dl_test(file))
    end
  end

  context "download com/mygroup test 7.0.0 -r #{nexus}" do
    it 'fails to download an absent artifact.' do
      out = 'Unable to download '\
        "#{nexus}/com/mygroup/test/7.0.0/test-7.0.0.jar: Status: 404\n"
      expect { start(self) }.to output(out).to_stderr
    end
  end

  context 'download com/mygroup test 7.0.0-SNAPSHOT' do
    it 'downloads a snapshot version by appending SNAPSHOT to the version.' do
      file = 'test-7.0.0-19831018.151200-1.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test latest' do
    it 'downloads latest released (actually rc1) version.' do
      file = 'test-7.0.0-rc1.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test latest -n' do
    it 'downloads latest snapshot version.' do
      file = 'test-7.0.0-19831018.151200-1.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test2 latest -n' do
    it 'downloads latest released version because no snapshot exists.' do
      file = 'test2-1.0.0.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end

  context 'download com/mygroup test3 latest' do
    it 'fails to download anything because no release exists.' do
      out = "No valid version found, try with --snapshot?\n"
      expect { start(self) }.to output(out).to_stderr
    end
  end

  context "download com/mygroup test latest -n -r #{redirect}" do
    it 'downloads on a repository that will redirect.' do
      file = 'test-7.0.0-19831018.151200-1.jar'
      out = "#{file} has been Downloaded.\n"
      expect { start(self) }.to output(out).to_stdout
      expect { print File.read(dl_test(file)) }.to output('OK').to_stdout
      File.delete(dl_test(file))
    end
  end
end
