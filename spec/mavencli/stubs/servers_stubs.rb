#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

meta_file = 'maven-metadata.xml'

def read_xml(name)
  meta_file = 'maven-metadata.xml'
  dir = File.dirname(__FILE__)
  File.read(File.join(dir, '..', '..', 'files', "#{name}_#{meta_file}"))
end

maven = 'https://repo.maven.apache.org/maven2'
nexus = 'https://nexus.mydomain.net/repository/maven-public'
redirect = 'https://toredirect.org/maven'

tests = {
  [maven, 'com/mygroup', 'test', 'root'] => [
    [200, '1.0.0', 'jar'],
    [200, '4.0.0', 'tgz'],
    [200, '5.0.0', 'jar'],
    [200, '6.0.0', 'jar'],
    [200, '7.0.0-rc1', 'jar'],
    [200, '7.0.0-SNAPSHOT', 'jar', '-19831018.151200-1']
  ],
  [nexus, 'com/mygroup', 'test', 'root'] => [
    [200, '3.0.0', 'jar'],
    [404, '7.0.0', 'jar']
  ],
  [maven, 'com/mygroup', 'test2', 'rel'] => [
    [200, '1.0.0', 'jar']
  ],
  [maven, 'com/mygroup', 'test3', 'snap'] => [
    [200, '1.0.0-SNAPSHOT', 'jar', '-19831018.151200-1']
  ]
}

RSpec.configure do |config|
  config.before(:each) do
    tests.each do |key, arrays|
      repo, group, artifact, root_xml = key

      # artifact metafile
      meta_path = "#{group}/#{artifact}/#{meta_file}"
      stub_request(:get, "#{redirect}/#{meta_path}")
        .to_return(status: 307, headers: { location: "#{repo}/#{meta_path}" })
      stub_request(:get, "#{repo}/#{meta_path}")
        .to_return(status: 200, body: read_xml(root_xml), headers: {})

      arrays.each do |code, version, type, tag|
        path = "#{group}/#{artifact}/#{version}"

        # version metafile
        meta = "#{group}/#{artifact}/#{version}/#{meta_file}"
        stub_request(:get, "#{redirect}/#{path}/#{meta_file}")
          .to_return(status: 302, headers: { location: "#{repo}/#{meta}" })

        xml_status = version.end_with?('SNAPSHOT') ? code : 404
        stub_request(:get, "#{repo}/#{meta}")
          .to_return(status: xml_status, body: read_xml('inside'), headers: {})

        # file download
        file_version = "#{version.chomp('-SNAPSHOT')}#{tag}"
        file = "#{path}/#{artifact}-#{file_version}.#{type}"
        stub_request(:get, "#{redirect}/#{file}")
          .to_return(status: 301, headers: { location: "#{repo}/#{file}" })
        stub_request(:get, "#{repo}/#{file}")
          .to_return(status: code, body: 'OK', headers: {})
      end
    end
  end
end
