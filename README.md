MavenCLI
========

Simple maven cli tool.

Build Status & Coverage Report
------------------------------

[![build status]
(https://gitlab.com/s4m-gems/maven-cli/badges/develop/build.svg)]
(https://gitlab.com/s4m-gems/maven-cli/commits/develop)
[![coverage report]
(https://gitlab.com/s4m-gems/maven-cli/badges/develop/coverage.svg)]
(https://s4m-gems.gitlab.io/maven-cli/)

Installation
------------

Install as a gem:

    $ gem install maven-cli

Usage
-----

An interactive help is available with:

    $ maven-cli help

Examples
--------

To download a jar from maven repository (http://repo.maven.apache.org/maven2)

    $ maven-cli download <group> <artifact> <version>

For instance: `maven-cli download com.google.guava guava 19.0`

To download the latest version of a jar, use `latest` as version:

    $ maven-cli download <group> <artifact> latest

To download a jar from a non standard repository

    $ maven-cli download <group> <artifact> <version> -r <repository>

To download a jar from maven repository and save it to file

    $ maven-cli download <group> <artifact> <version> -o <outfile>

More options with

    $ maven-cli help download

Contributing
------------

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

License and Author
------------------

- Author:: DPS Team (<dps.team@s4m.io>)
- Author:: Samuel Bernard (<samuel.bernard@s4m.io>)
- Author:: Richard Delaplace (<richard.delaplace@s4m.io>)

```text
Copyright (c) 2016 Sam4Mobile

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
