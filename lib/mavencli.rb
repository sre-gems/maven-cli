#!/urs/bin/ruby
#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Maven CLI class
module MavenCLI
  class << self
    Dir[File.join(File.dirname(__FILE__), '*', '*.rb')].each do |file|
      require file
    end

    def start(args = ARGV, test = false)
      MavenCLI::CLI.start(args)
    rescue => e
      $stderr.puts e
      exit(1) unless test # Do not exit if we are in a test
    end
  end
end
