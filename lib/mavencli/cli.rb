#!/usr/bin/ruby
#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'

module MavenCLI
  # Simple CLI for maven_download
  class CLI < Thor
    desc 'version', 'Print maven-cli current version'
    def version
      puts "MavenCLI version #{MavenCLI::VERSION}"
    end

    desc('download GROUP ARTIFACT VERSION [options]',
         'Download an artifact from a maven repository. VERSION can be set '\
         'to "latest" to fetch the latest release or snapshot (if --snapshot)'\
         ' available.')
    option(
      :repository,
      aliases: ['-r'],
      default: 'https://repo.maven.apache.org/maven2',
      desc: 'Requested repository. For exemple, http://myserver.com/myrepo'
    )
    option(
      :classifier,
      aliases: ['-c'],
      desc: 'Classifier of the requested artifact'
    )
    option(
      :packaging,
      aliases: ['-p'],
      default: 'jar',
      desc: 'Packaging of the requested artifact'
    )
    option(
      :output_file,
      aliases: ['-o'],
      desc: 'Output file'
    )
    option(
      :quiet,
      aliases: ['-q'],
      type: :boolean,
      default: false,
      desc: 'Silently do the job'
    )
    option(
      :simulation,
      aliases: ['-s'],
      type: :boolean,
      default: false,
      desc: 'Simulation mode. Do nothing'
    )
    option(
      :progress,
      aliases: ['-d'],
      type: :boolean,
      default: true,
      desc: 'Display a progressbar.'
    )
    option(
      :snapshot,
      aliases: ['-n'],
      type: :boolean,
      default: false,
      desc: 'Allow the download of a SNAPSHOT version. Automatically set if '\
      'VERSION is a SNAPSHOT version'
    )
    def download(group, artifact, version)
      opts = options.dup
      opts['progress'] &&= !opts['quiet']
      downloader = MavenCLI::Downloader.new(group, artifact, version, opts)
      downloader.download
    end
  end
end
