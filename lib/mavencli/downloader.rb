#!/usr/bin/ruby
#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'stringio'
require 'net/http'
require 'uri'
require 'xmlsimple'
require 'ruby-progressbar'
require 'devnull'

module MavenCLI
  # This is a simple file downloader class.
  class Downloader
    def initialize(group, artifact, version, opts)
      path = "#{group.tr('.', '/')}/#{artifact}"
      version = latest_version(opts, path) if version.casecmp('latest').zero?
      fullpath = "#{path}/#{version}"
      artifact_name = filename(artifact, version, opts, fullpath)
      @target = "#{opts['repository']}/#{fullpath}/#{artifact_name}"
      @outfile = opts['output_file'].nil? ? artifact_name : opts['output_file']
      @opts = opts
    end

    def download
      get_write(@target, @outfile) unless @opts['simulation']
      puts "#{@outfile} has been Downloaded." unless @opts['quiet']
    end

    private

    def latest_version(opts, path)
      xml = get_write("#{opts['repository']}/#{path}/maven-metadata.xml")
      latest = xml_path(xml, 'versioning', 'latest')
      release = xml_path(xml, 'versioning', 'release')
      version = opts['snapshot'] && !latest.nil? ? latest : release
      raise 'No valid version found, try with --snapshot?' if version.nil?
      version
    end

    def filename(artifact, version, opts, path)
      name = "#{artifact}-#{version.chomp('-SNAPSHOT')}"
      if version.end_with?('SNAPSHOT')
        xml = get_write("#{opts['repository']}/#{path}/maven-metadata.xml")
        name += snapshot_tag(xml)
      end
      name += "-#{opts['classifier']}" unless opts['classifier'].nil?
      name += ".#{opts['packaging']}"
      name
    end

    def snapshot_tag(xml)
      node = xml_path(xml, 'versioning', 'snapshot')
      "-#{node['timestamp'].first}-#{node['buildNumber'].first}"
    end

    def xml_path(xml, *path)
      path.reduce(XmlSimple.xml_in(xml)) { |acc, elem| acc[elem].first }
    rescue
      nil
    end

    def get_write(target, outfile = '')
      ssl = target.start_with?('https://')
      io = (outfile.empty? ? StringIO : File).new(outfile.dup, 'w')
      response = get_write_response(URI(target), ssl, io, outfile)
      io.close
      case response
      when Net::HTTPSuccess then io.is_a?(StringIO) ? io.string : 'ok'
      when Net::HTTPRedirection then get_write(response['location'], outfile)
      else raise "Unable to download #{target}: Status: #{response.code}"
      end
    end

    def get_write_response(uri, ssl, io, outfile)
      Net::HTTP.start(uri.host, uri.port, use_ssl: ssl) do |http|
        request = Net::HTTP::Get.new(uri)
        http.request(request) do |response|
          if response.is_a?(Net::HTTPSuccess)
            write_response(io, outfile, response)
          end
        end
      end
    end

    def write_response(io, outfile, response)
      progressbar = create_progressbar(response, outfile)
      response.read_body do |chunk|
        io.write(chunk)
        progressbar.progress += chunk.length / 1024
      end
      progressbar.finish
    end

    def create_progressbar(response, out)
      size = response['content-length'].to_i / 1024
      options = {
        title: out.length > 19 ? "#{out[0..14]}…#{out[-4..-1]}" : out,
        total: size.zero? ? nil : size,
        output: out.empty? || !@opts['progress'] ? DevNull.new : $stdout,
        format: "%t %b\u{15E7}%i %RkB/s %p%% %E",
        progress_mark: ' ',
        remainder_mark: "\u{FF65}"
      }
      ProgressBar.create(options)
    end
  end
end
